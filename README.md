# tinydns-json

## Introduction

This script takes one or more TinyDNS records, and outputs a JSON representation of those records.

This was written to solve a specific problem which I had, so it only supports a subset of the full TinyDNS record format. It's trivial to add further record types. The currently supported record types are:

- nameserver records
- hostname records
- alias records
- MX records
- TXT records

## Usage

	Usage: tinydns-json.py [OPTIONS] INPUT... OUTPUT

	  Parses the list of given <INPUT> files, and writes the JSON to the
	  <OUTPUT> file.

	Options:
	  --help  Show this message and exit.

## Output

The script outputs JSON, which looks something like this:

	{
	    "example.com": [
	        {
	            "priority": false,
	            "type": "NS",
	            "name": "example.com",
	            "value": "a.nameservers.com",
	            "ttl": false
	        },
	        {
	            "priority": "10",
	            "type": "MX",
	            "name": "example.com",
	            "value": "a.mailserver.com",
	            "ttl": false
	        },
			...
		],
		...
	}

## Credits

This was heavily inspired by [this](http://offog.org/git/misccode/tinydns-to-zones) uncredited script, possibly authored by [Adam Sampson](http://offog.org/). I ended up writing my own (limited) parser, but this code gave me the starting point.
