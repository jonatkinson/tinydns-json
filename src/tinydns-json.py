import click
import json


class DNSRecord(object):
    """
    This is a single DNS record.
    """

    def __init__(self, domain=False, type=False, value=False, priority=False, ttl=False):
        self.domain = domain
        self.type = type
        self.value = value
        self.priority = priority
        self.ttl = ttl

    def __unicode__(self):
        return "%s: %s" % (self.type, self.value)


class TinyDNSParser(object):
    """
    This parses TinyDNS records. In a very limited manner.
    It is written by someone who does not really know how to write a parser.
    """

    def __init__(self, filename):
        self.file = open(filename, "r")

    def nameserver_record(self, line):
        """
        Extracts nameserver records, in the form of:
        .domain:ip:xname:ttl:tai64:loc
        """

        parts = line[1:].rstrip().split(":")
        return DNSRecord(domain=parts[0], type="NS", value=parts[2])

    def hostname_record(self, line):
        """
        Extracts hostname records, in the form of:
        =hostname:ip:ttl:tai64:loc
        """

        parts = line[1:].rstrip().split(":")
        return DNSRecord(domain=parts[0], type="A", value=parts[1])

    def alias_record(self, line):
        """
        Extracts an alias record, in the form of:
        +hostname:ip:ttl:tai64:loc
        """

        parts = line[1:].rstrip().split(":")
        return DNSRecord(domain=parts[0], type="A", value=parts[1])

    def mx_record(self, line):
        """
        Extracts an MX record, in the form of:
        @domain:ip:xname:distance:ttl:tai64:loc
        """

        parts = line[1:].rstrip().split(":")
        record = DNSRecord(domain=parts[0], type="MX", value=parts[2])

        if len(parts) > 3:
            record.priority = parts[3]

        if len(parts) > 4:
            record.ttl = parts[4]

        return record

    def txt_record(self, line):
        """
        Extracts a text record, in the form of:
        'fqdn:string:ttl:tai64:loc
        """

        parts = line[1:].rstrip().split(":")
        record = DNSRecord(domain=parts[0], type="TXT", value=parts[1])

        if len(parts) > 2:
            record.ttl = parts[2]

        return record

    def parse(self):
        """
        Opens a given tinyDNS file, and parses the records.
        """

        records = []

        # Loop through the lines in the file. Parse them as appropriate.
        for line in self.file:

            # Skip comments.
            if line.startswith("#"):
                continue

            # Parse nameserver records.
            if line.startswith("."):
                records.append(self.nameserver_record(line))

            # Parse hostname records.
            if line.startswith("="):
                records.append(self.hostname_record(line))

            # Parse alias record.
            if line.startswith("+"):
                records.append(self.alias_record(line))

            # Parse MX record.
            if line.startswith("@"):
                records.append(self.mx_record(line))

            # Parse MX record.
            if line.startswith("'"):
                records.append(self.txt_record(line))

        return records


@click.command()
@click.argument('input', nargs=-1)
@click.argument('output', default="output.json", help="Output file location")
def run(input, output):
    """
    Parses the list of given <INPUT> files, and writes the JSON to the <OUTPUT> file.
    """

    records = {}

    # Loop through each file in turn...
    for f in input:

        # Setup the parser, then parse the file.
        p = TinyDNSParser(f)
        records_for_file = p.parse()

        # Loop through the parsed records.
        for r in records_for_file:

            # Create the dict entry for the domain, if it doesn't already exist.
            if r.domain not in records:
                records[r.domain] = []

            # Append the record information to the dict.
            records[r.domain].append({
                "type": r.type,
                "value": r.value,
                "name": r.domain,
                "ttl": r.ttl,
                "priority": r.priority
            })

    # Write out the JSON.
    with open(output, 'w') as outfile:
        json.dump(records, outfile)


if __name__ == '__main__':
    run()